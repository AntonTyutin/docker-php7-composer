FROM php:7.0-alpine

MAINTAINER Anton Tyutin <anton@tyutin.ru>

RUN apk add --no-cache --virtual .phpextdeps libpng-dev jpeg-dev freetype-dev libmcrypt-dev \
    && apk add --no-cache binutils \
    && docker-php-ext-install mcrypt pdo_mysql gd zip \
    && strip usr/local/lib/php/extensions/*/*.so \
    && apk del binutils && rm -rf /tmp/*

RUN apk add --no-cache git \
    && php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
    && [ "$(curl -qsS https://composer.github.io/installer.sig)" == "$(php -r "echo hash_file('SHA384', 'composer-setup.php');")" ] \
    && php composer-setup.php --quiet && rm composer-setup.php && mv composer.phar /usr/bin/composer.phar \
    && rm -rf /tmp/*

ENV COMPOSER_HOME=/.composer

RUN mkdir -pm 777 "$COMPOSER_HOME"

ENTRYPOINT ["/usr/bin/composer.phar"]